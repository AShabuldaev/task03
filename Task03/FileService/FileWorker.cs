﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03.FileService
{
    public class FileWorker
    {
        private string FilePath { get; set; }

        public FileWorker(string path)
        {
            FilePath = path;
        }

        public string ReadFile()
        {
            using (StreamReader strR = new StreamReader(FilePath, Encoding.Default))
            {
                return strR.ReadToEnd();
            }
        }

        public IEnumerable<Purchase> CSVConvert(string CSVString)
        {
            List<Purchase> outpArr = new List<Purchase>();
            var list = CSVString.Split('\r');
            foreach (var a in list)
            {
                var row = a.Split(';');
                var purch = new Purchase()
                {
                    Cost = Convert.ToDecimal(row[0]),
                    CreditCardID = Convert.ToInt32(row[1]),
                    PurchaseDate = Convert.ToInt32(row[2]),
                    ShopName = row[3]
                };
                outpArr.Add(purch);
            }
            return outpArr;
        }
    }
}

