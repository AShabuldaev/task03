﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task03.FileService;

namespace Task03
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = args[0];
            FileWorker fileWorker = new FileWorker(path);
            var purchases = fileWorker.CSVConvert(fileWorker.ReadFile());

            foreach (var a in purchases)
                a.PrintPurchase();

            Console.WriteLine("Ordered by date");

            foreach (var a in purchases.OrderByDescending(t => t.PurchaseDate))
                a.PrintPurchase();

            Console.WriteLine("step3");
            foreach (var a in purchases.GroupBy(t => t.CreditCardID).OrderBy(t => t.Key))
            {
                decimal sum = 0;
                foreach (var c in a)
                     sum += c.GetDiscountedCost();

                Console.WriteLine(a.Key.ToString() + " sum :" + sum);
            }

            Console.ReadLine();
        }
    }
}
